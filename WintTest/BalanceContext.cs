﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WintTest.Models;

namespace WintTest
{
    public class BalanceContext
    {
        private List<Func<Transaction, bool>> Filters { get; set; } = new List<Func<Transaction, bool>>();
        private IEnumerable<Transaction> Transactions { get; set; }
        private decimal InitialBalance { get; set; }

        public List<Transaction> FilteredTransactions
        {
            get
            {
                return this.Filters
                    .Aggregate(
                        Transactions,
                        (current, predicate) => current.Where(t => predicate(t))
                    ).ToList();
            }
        }

        private BalanceContext(decimal initialBalance, IEnumerable<Transaction> transactions, List<Func<Transaction, bool>> filters)
        {
            this.InitialBalance = initialBalance;
            this.Filters = filters;
            this.Transactions = transactions;
        }

        public BalanceContext() { }

        public BalanceContext WithInitialBalance(decimal initialBalance)
        {
            return new BalanceContext(initialBalance, this.Transactions, this.Filters);
        }

        public BalanceContext WithTransactions(List<Transaction> transactions)
        {
            return new BalanceContext(this.InitialBalance, transactions, this.Filters);
        }

        public BalanceContext WithFilters(List<Func<Transaction, bool>> filters)
        {
            return new BalanceContext(this.InitialBalance, this.Transactions, filters);
        }

        public BalanceContext WithFilter(Func<Transaction, bool> filter)
        {
            return new BalanceContext(this.InitialBalance, this.Transactions, new List<Func<Transaction, bool>>() { filter });
        }

        public decimal GetBalance()
        {
            return InitialBalance + FilteredTransactions
                .Sum(t => t.Amount);
        }
    }
}