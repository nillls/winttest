﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WintTest.Models
{
    public class Transaction
    {
        public decimal Amount { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
