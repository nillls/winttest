﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WintTest.Models;
using WintTest.Properties;

namespace WintTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var accountModel = JsonConvert.DeserializeObject<Account>(Encoding.UTF8.GetString(Resources.bankaccount));

            var balanceBuilder = new BalanceContext()
                .WithInitialBalance(accountModel.InitialBalance)
                .WithTransactions(accountModel.Transactions);

            var balanceAtDate = balanceBuilder
                .WithFilter(t => t.TimeStamp <= new DateTime(2015, 09, 04, 12, 00, 00))
                .GetBalance();

            Console.WriteLine($"Balance at 2015-09-04 12:00 was {balanceAtDate}");

            var positiveSum = balanceBuilder
                .WithFilter(t => t.Amount > 0)
                .FilteredTransactions
                .Sum(t => t.Amount);

            var negativeSum = balanceBuilder
                .WithFilter(t => t.Amount < 0)
                .FilteredTransactions
                .Sum(t => t.Amount);

            Console.WriteLine($"Sum of all positive transactions is: {positiveSum}");
            Console.WriteLine($"Sum of all negative transactions is: {negativeSum}");

            var grouped = balanceBuilder
                .FilteredTransactions
                .GroupBy(t => t.TimeStamp.Date)
                .OrderBy(t => t.Key);

            foreach (var transactions in grouped)
            {
                var balanceByDate = balanceBuilder
                    .WithFilter(t => t.TimeStamp.Date <= transactions.Key.Date)
                    .GetBalance();

                Console.WriteLine($"The balance at {transactions.Key.ToString("yyyy-MM-dd")} was {balanceByDate}");
            }

            Console.ReadLine();
        }
    }
}